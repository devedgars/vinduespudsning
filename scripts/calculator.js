$(document).ready(function() {
    var doubleModOn = 0;
    var doubleModOn2 = 0;
    var stampMod = 0;
    var basePrice = 300;
    var totalPrice = 0;
    var windowCollection = {
        "priceCollection": {
            "outside": {
                "1": 20,
                "2": 25,
                "3": 35,
                "4": 40,
                "5": 40,
                "6": 45,
                "7": 35,
                "8": 50,
                "9": 55,
                "10": 60,
                "11": 60,
                "12": 65,
                "13": 50,
                "14": 60,
                "15": 65,
                "16": 65,
                "17": 90,
                "18": 95,
                "19": 55,
                "20": 79,
                "21": 65,
                "22": 45,
                "23": 35,
                "24": 40,
                "25": 20,
                "26": 40,
                "27": 50,
                "28": 25,
                "29": 50,
                "30": 70,
                "31": 35,
                "32": 60,
                "33": 80,
                "34": 25,
                "35": 45,
                "36": 65,
                "37": 27,
                "38": 60,
                "39": 75,
                "40": 32,
                "41": 55,
                "42": 25,
                "43": 52,
                "44": 85,
                "45": 22,
                "46": 25
            },
            "both": {
                "1": 30,
                "2": 40,
                "3": 45,
                "4": 55,
                "5": 60,
                "6": 65,
                "7": 50,
                "8": 70,
                "9": 75,
                "10": 87,
                "11": 90,
                "12": 95,
                "13": 70,
                "14": 95,
                "15": 95,
                "16": 95,
                "17": 110,
                "18": 140,
                "19": 85,
                "20": 115,
                "21": 85,
                "22": 60,
                "23": 45,
                "24": 50,
                "25": 30,
                "26": 50,
                "27": 75,
                "28": 40,
                "29": 65,
                "30": 90,
                "31": 45,
                "32": 80,
                "33": 100,
                "34": 35,
                "35": 65,
                "36": 90,
                "37": 40,
                "38": 75,
                "39": 95,
                "40": 45,
                "41": 72,
                "42": 35,
                "43": 70,
                "44": 110,
                "45": 35,
                "46": 40
            }
        },
        "qtyCollection": {
            "1": {
                "qty": 0
            }, "2": {
                "qty": 0
            }, "3": {
                "qty":0
            },  "4": {
                "qty": 0
            },  "5": {
                "qty": 0
            },  "6": {
                "qty": 0
            },  "7": {
                "qty": 0
            },  "8": {
                "qty": 0
            },  "9": {
                "qty": 0
            },  "10": {
                "qty": 0
            },  "11": {
                "qty": 0
            },  "12": {
                "qty": 0
            },  "13": {
                "qty": 0
            },  "14": {
                "qty": 0
            },  "15": {
                "qty": 0
            },  "16": {
                "qty": 0
            },  "17": {
                "qty": 0
            },  "18": {
                "qty": 0
            },  "19": {
                "qty": 0
            },  "20": {
                "qty": 0
            },  "21": {
                "qty": 0
            },  "22": {
                "qty": 0
            },  "23": {
                "qty": 0
            },  "24": {
                "qty": 0
            },  "25": {
                "qty": 0
            },  "26": {
                "qty": 0
            },  "27": {
                "qty": 0
            },  "28": {
                "qty": 0
            },  "29": {
                "qty": 0
            },  "30": {
                "qty": 0
            },  "31": {
                "qty": 0
            },  "32": {
                "qty": 0
            },  "33": {
                "qty": 0
            },  "34": {
                "qty": 0
            },  "35": {
                "qty": 0
            },  "36": {
                "qty": 0
            },  "37": {
                "qty": 0
            },  "38": {
                "qty": 0
            },  "39": {
                "qty": 0
            },  "40": {
                "qty": 0
            },  "41": {
                "qty": 0
            },  "42": {
                "qty": 0
            },  "43": {
                "qty": 0
            },  "44": {
                "qty": 0
            },  "45": {
                "qty": 0
            },  "46": {
                "qty": 0
            }

        },
        "nameCollection": {
            "1": {
                "name": "1 fags vindue-1(No complexity)",
                "type": 1
            }, "2": {
                "name": "1 fags vindue-2(Small complexity)",
                "type": 1
            }, "3": {
                "name": "1 fags vindue-3(Medium complexity)",
                "type": 1
            },  "4": {
                "name": "1 fags vindue-4(Medium complexity)",
                "type": 1
            },  "5": {
                "name": "1 fags vindue-5(Medium complexity)",
                "type": 1
            },  "6": {
                "name": "1 fags vindue-6(Advanced complexity)",
                "type": 1
            },  "7": {
                "name": "2 fags vindue-1(No complexity)",
                "type": 2
            },  "8": {
                "name": "2 fags vindue-2(Small complexity)",
                "type": 2
            },  "9": {
                "name": "2 fags vindue-3(Medium complexity)",
                "type": 2
            },  "10": {
                "name": "2 fags vindue-4(Medium complexity)",
                "type": 2
            },  "11": {
                "name": "2 fags vindue-5(Advanced complexity)",
                "type": 2
            },  "12": {
                "name": "2 fags vindue-6(Advanced complexity)",
                "type": 2
            },  "13": {
                "name": "3 fags vindue-1(No complexity)",
                "type": 3
            },  "14": {
                "name": "3 fags vindue-2(Small complexity)",
                "type": 3
            },  "15": {
                "name": "3 fags vindue-3(Small complexity)",
                "type": 3
            },  "16": {
                "name": "3 fags vindue-4(Medium complexity)",
                "type": 3
            },  "17": {
                "name": "3 fags vindue-5(Advanced complexity)",
                "type": 3
            },  "18": {
                "name": "3 fags vindue-6(Advanced complexity)",
                "type": 3
            },  "19": {
                "name": "Special panel window-1(Medium complexity)",
                "type": 4
            },  "20": {
                "name": "3 fags vindue-7(Medium complexity)",
                "type": 3
            },  "21": {
                "name": "Special panel window-2(Small complexity)",
                "type": 4
            },  "22": {
                "name": "Special panel window-3(Medium complexity)",
                "type": 4
            },  "23": {
                "name": "Special panel window-4(Small complexity)",
                "type": 4
            },  "24": {
                "name": "Special panel window-5(Small complexity)",
                "type": 4
            },  "25": {
                "name": "1 fags vindue-7(No complexity)",
                "type": 1
            },  "26": {
                "name": "2 fags vindue-7(No complexity)",
                "type": 2
            },  "27": {
                "name": "3 fags vindue-8(No complexity)",
                "type": 3
            },  "28": {
                "name": "1 fags vindue-8(Small complexity)",
                "type": 1
            },  "29": {
                "name": "2 fags vindue-8(Small complexity)",
                "type": 2
            },  "30": {
                "name": "3 fags vindue-9(Small complexity)",
                "type": 3
            },  "31": {
                "name": "1 fags vindue-9(Medium complexity)",
                "type": 1
            },  "32": {
                "name": "2 fags vindue-9(Medium complexity)",
                "type": 2
            },  "33": {
                "name": "3 fags vindue-10(Medium complexity)",
                "type": 3
            },  "34": {
                "name": "1 fags vindue-10(Advanced complexity)",
                "type": 1
            },  "35": {
                "name": "2 fags vindue-10(Advanced complexity)",
                "type": 2
            },  "36": {
                "name": "3 fags vindue-11(Advanced complexity)",
                "type": 3
            },  "37": {
                "name": "1 fags vindue-11(Medium complexity)",
                "type": 1
            },  "38": {
                "name": "2 fags vindue-11(Medium complexity)",
                "type": 2
            },  "39": {
                "name": "3 fags vindue-12(Medium complexity)",
                "type": 3
            },  "40": {
                "name": "Speciel vindue-6(Medium complexity)",
                "type": 4
            },  "41": {
                "name": "Speciel vindue-7(Medium complexity)",
                "type": 4
            },  "42": {
                "name": "1 fags vindue-13(Small complexity)",
                "type": 1
            },  "43": {
                "name": "2 fags vindue-12(Small complexity)",
                "type": 2
            },  "44": {
                "name": "Speciel vindue-9(Medium complexity)",
                "type": 4
            },  "45": {
                "name": "Speciel vindue-10(Small complexity)",
                "type": 4
            },  "46": {
                "name": "Speciel vindue-11(Small complexity)",
                "type": 4
            }
        }
    };

    function getWindowData(window) {
        selectedWindow = $(window).closest(".window");
        windowId = selectedWindow.data("window");
        priceCollection = windowCollection.priceCollection;
        qtyCollection = windowCollection.qtyCollection;
    }

    // Recalculate prices
    function recalculate() {
        // $('.remove').remove();
        var finalOrder = {};
        finalOrder.products = [];
        finalOrder.details = [];
        finalOrder.qty = {
            "singlePanel": 0,
            "doublePanel": 0,
            "triplePanel": 0,
            "specialPanel": 0
        };
        var totalPrice = 0;
        var qtyCollection = windowCollection.qtyCollection;
        var nameCollection = windowCollection.nameCollection;
        var productText = "";

        if (doubleModOn) {
            for (var x in windowCollection.priceCollection.both) {
                var qty = qtyCollection[x].qty;
                if (qty > 0) {
                    var windowPrice = windowCollection.priceCollection.both[x];
                    var fullPrice = windowPrice * qty;
                    finalOrder.products.push({
                        "name" : nameCollection[x].name,
                        "qty" : qty,
                        "windowprice" : windowPrice,
                        "fullprice" : fullPrice
                    });
                    switch(nameCollection[x].type){
                        case 1:
                            finalOrder.qty.singlePanel += qty;
                            break;
                        case 2:
                            finalOrder.qty.doublePanel += qty;
                            break;
                        case 3:
                            finalOrder.qty.triplePanel += qty;
                            break;
                        case 4:
                            finalOrder.qty.specialPanel += qty;
                            break;
                    }
                    totalPrice = totalPrice + fullPrice;
                }
                // $(".window[data-window='" + x + "']").append("<div class='remove' style='position: absolute; color: red; font-size: 24px; z-index: 999; bottom: 0;'>Both: "+windowPrice+"DKK</div>")

            }
        } else if (!doubleModOn) {
            for (var x in windowCollection.priceCollection.outside) {
                var qty = qtyCollection[x].qty;
                if (qty > 0) {
                    var windowPrice = windowCollection.priceCollection.outside[x];
                    var fullPrice = windowPrice * qty;
                    finalOrder.products.push({
                        "name" : nameCollection[x].name,
                        "qty" : qty,
                        "windowprice" : windowPrice,
                        "fullprice" : fullPrice
                    });

                    switch(nameCollection[x].type){
                        case 1:
                            finalOrder.qty.singlePanel += qty;
                            break;
                        case 2:
                            finalOrder.qty.doublePanel += qty;
                            break;
                        case 3:
                            finalOrder.qty.triplePanel += qty;
                            break;
                        case 4:
                            finalOrder.qty.specialPanel += qty;
                            break;
                    }
                    totalPrice = totalPrice + fullPrice;
                }
                // $(".window[data-window='" + x + "']").append("<div class='remove' style='position: absolute; color: red; font-size: 24px; z-index: 999; bottom: 0;'>Single: "+windowPrice+"DKK</div>");
            }
        }
        if (totalPrice < basePrice) {
            totalPrice = basePrice
        }

        finalOrder.details.totalPrice = totalPrice;
        finalOrder.details.doubleMod = (doubleModOn == false ? 'Kun udvendig' : 'Indvendig og udvendig');
        finalOrder.details.doubleMod2 = (doubleModOn2 == false ? 'Enkelt terrassedør' : 'Dobbelt terrassedør');
        finalOrder.details.stampMod = (stampMod == false ? 'Intet klip' : 'købe klip');

        if (totalPrice == 0) {
            $(".calcualtor-bottom > h4").html("Vælg vinduer for udregning af pris");
        } else {
            console.log(finalOrder.qty);
            $(".calcualtor-bottom > h4").html("du har valgt: "+
                                (finalOrder.qty.singlePanel > 0? " " + finalOrder.qty.singlePanel + " 1-fags vinduer <br>": "")+
                                (finalOrder.qty.doublePanel > 0? " " + finalOrder.qty.doublePanel + " 2-fags vinduer <br>": "")+
                                (finalOrder.qty.triplePanel > 0? " " + finalOrder.qty.triplePanel + " 3-fags vinduer <br>": "")+
                                (finalOrder.qty.specialPanel > 0? " " + finalOrder.qty.specialPanel + " Special vinduer <br>": "")+
                                "Og din pris for vinduespudsning er:");
        }
        if(doubleModOn2 == true) {
            totalPrice = totalPrice*1.5;
        }
        if(stampMod) {
            totalPrice = totalPrice + 100;
        }
        $(".mods").html((doubleModOn || doubleModOn2 || stampMod? " i prisen inkluderet: <br>": "")+
                        (doubleModOn? " *begge sider vaskes<br>": "")+
                        (doubleModOn2? " *pudsning forsatsvinduer <br>og tilbage vinduer": "")+
                        (stampMod? " *loyalitets clips": ""));
        $(".calcualtor-bottom > .price").html(totalPrice+"DKK");

        var arrayLength = finalOrder.products.length;
        for (var x in finalOrder.products) {
            if (x <= arrayLength) {
                var count = Number(x) + 1;
                productText = productText + "\\\n" + count + ") " +
                            "Navn: "+ finalOrder.products[x].name +
                            ", Pris per vindue: " + finalOrder.products[x]['windowprice'] +
                            ", Antal: "+ finalOrder.products[x]['qty'] +
                            ", Pris ialt: "+ finalOrder.products[x]['fullprice'];
            }
        }
        $("#textarea-yui_3_17_2_1_1530877888783_52766-field").val(productText);
        $("input[name='SQF_BOTH_SIDE']").val((finalOrder.details.doubleMod));
        $("input[name='SQF_DOUBLE_WINDOW']").val((finalOrder.details.doubleMod2));
        $("input[name='SQF_BUYING_LOYALTY_STAMP']").val((finalOrder.details.stampMod));
        $("input[name='SQF_TOTAL_PRICE']").val(totalPrice + "DKK");

    }

    // Apply outside or both modifier
    $(".double-mod").click(function(){
        doubleModOn = $(this).data("enabled");
        if (!doubleModOn) {
            doubleModOn = 1;
            $(this).removeClass("no");
            $(this).data("enabled", doubleModOn);
            $(this).html("Ja");
        } else if (doubleModOn) {
            doubleModOn = 0;
            $(this).addClass("no");
            $(this).data("enabled", doubleModOn);
            $(this).html("Nej");
        }
        // windowPrices
        recalculate();
    });

    // Apply double mod modifier
    $(".double-mod-2").click(function(){
        doubleModOn2 = $(this).data("enabled");
        console.log(doubleModOn2);
        if (!doubleModOn2) {
            doubleModOn2 = 1;
            $(this).removeClass("no");
            $(this).data("enabled", doubleModOn2);
            $(this).html("Ja");
        } else if (doubleModOn2) {
            doubleModOn2 = 0;
            $(this).addClass("no");
            $(this).data("enabled", doubleModOn2);
            $(this).html("Nej");
        }
        // windowPrices
        recalculate();
    });

    // Apply Stamp Mod
    $(".stamp-mod").click(function(){
        stampMod = $(this).data("enabled");
        if (!stampMod) {
            stampMod = 1;
            $(this).removeClass("no");
            $(this).data("enabled", stampMod);
            $(this).html("Ja");
        } else if (stampMod) {
            stampMod = 0;
            $(this).addClass("no");
            $(this).data("enabled", stampMod);
            $(this).html("Nej");
        }
        // windowPrices
        recalculate();
    });

    //Add qty for window
    $(".w-plus, .image-holder").click(function () {

        getWindowData(this);
        qtyCollection[windowId].qty++;
        var qty = qtyCollection[windowId].qty;
        if (qty > 0) {
            $(selectedWindow).find(".qty").html(qty);
            $(selectedWindow).find(".qty").removeClass("hidden");
            $(this).closest(".window").addClass("selected");
        }
        // windowPrices
        recalculate();
    });

    //Remove qty for window
    $(".w-minus").click(function () {

        getWindowData(this);
        if (qtyCollection[windowId].qty > 0) {
            qtyCollection[windowId].qty--;
            var qty = qtyCollection[windowId].qty;
            if (qty == 0) {
                $(selectedWindow).find(".qty").html(qty);
                $(selectedWindow).find(".qty").addClass("hidden");
                $(this).closest(".window").removeClass("selected");
            } else {
                $(selectedWindow).find(".qty").html(qty);
            }
            // windowPrices
            recalculate();
        }
    });

    //control category opening
    $(".section-heading").click(function (){
        var category = $(this).closest(".section").find(".window-container");
        var arrow = $(this).find(".arrow");
       if(category.hasClass("closed")) {
           category.show( function(){
               category.css("display", "flex");
               category.removeClass("closed");
           });
           arrow.addClass("down");
       } else {
           $(category).hide( function(){
               category.addClass("closed");
           });
           arrow.removeClass("down");
       }
    });

    //Loaylty Stamps po up
    $('.stamp').magnificPopup({
        items: {
            src: $('<div class="white-popup">Dynamically created popup</div>'),
            type: 'inline'
        }
    });
});